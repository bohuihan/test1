import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserService } from '../user.service'
import { environment } from '../../environments/environment';

const baseUrl = environment.baseUrl + '/api/player/?format=json';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  httpOptions: any;

  constructor(private http: HttpClient, private _userService: UserService) {
  }

  updateHeader() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + this._userService.token
      })
    };
  }

  get(pk: number): Observable<any> {
    this.updateHeader();
    return this.http.get(`${baseUrl}${pk}`, this.httpOptions);
  }

  getByTeam(team_pk: number, limit: number = 0): Observable<any> {
    this.updateHeader();
    return this.http.get(`${baseUrl}&team_id=${team_pk}&percentile=${limit}`, this.httpOptions);
  }
}
