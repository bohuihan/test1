import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserService } from '../user.service'
import { environment } from '../../environments/environment';

const baseUrl = environment.baseUrl + '/api/player_status/?format=json';

@Injectable({
  providedIn: 'root'
})
export class PlayerStatusService {

  httpOptions: any;

  constructor(private http: HttpClient, private _userService: UserService) {
  }

  updateHeader() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + this._userService.token
      })
    };
  }

  get(): Observable<any> {
    this.updateHeader();
    return this.http.get(baseUrl, this.httpOptions);
  }

}
