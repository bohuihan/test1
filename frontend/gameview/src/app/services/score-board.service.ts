import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserService } from '../user.service'
import { environment } from '../../environments/environment';

const baseUrl = environment.baseUrl + '/api/game/?format=json';

@Injectable({
  providedIn: 'root'
})
export class ScoreBoardService {

  httpOptions: any;

  constructor(private http: HttpClient, private _userService: UserService) {
  }
  updateHeader() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'JWT ' + this._userService.token
      })
    };
  }

  getAll(): Observable<any> {
    this.updateHeader();
    return this.http.get(baseUrl, this.httpOptions);
  }

  byTeam(teamId: number): Observable<any> {
    this.updateHeader();
    return this.http.get(`${baseUrl}?byTeam=${teamId}`, this.httpOptions);
  }

  byPlayer(playerId: number): Observable<any> {
    this.updateHeader();
    return this.http.get(`${baseUrl}?byTeam=${playerId}`, this.httpOptions);
  }
}
