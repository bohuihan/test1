import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from "rxjs/Rx";

import { environment } from '../environments/environment';

import { WebsocketService } from "./websocket.service";

@Injectable()
export class UserService {

  // http options used for making API calls
  private httpOptions: any;

  // the actual JWT token
  public token: string;

  // the token expiration date
  public token_expires: Date;

  // the username of the logged in user
  public username: string;

  // error messages received from the login attempt
  public errors: any = [];

  public ws_messages: any;

  constructor(private http: HttpClient, private wsService: WebsocketService) {
    this.httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
  }

  // Uses http.post() to get an auth token from djangorestframework-jwt endpoint
  public login(user) {
    this.http.post(`${environment.baseUrl}/api-token-auth/`, JSON.stringify(user), this.httpOptions).subscribe(
      data => {
        this.updateData(data['token']);
        console.log('hahaha')
        this.ws_messages = this.wsService.connect(`${environment.wsBaseUrl}/ws/`).map(
          (response: any): any => {
            let data = JSON.parse(response.data);
            // console.log({ data });
            return {
              author: data.author,
              message: data.message
            };
          }
        );
      },
      err => {
        this.errors = err['error'];
      }
    );
  }

  // Refreshes the JWT token, to extend the time the user is logged in
  public refreshToken() {
    this.http.post(`${environment.baseUrl}/api-token-refresh/`, JSON.stringify({ token: this.token }), this.httpOptions).subscribe(
      data => {
        this.updateData(data['token']);
      },
      err => {
        this.errors = err['error'];
      }
    );
  }

  public logout() {
    this.token = null;
    this.token_expires = null;
    this.username = null;
    this.ws_messages.next('stop')
  }

  private updateData(token) {
    this.token = token;
    this.errors = [];

    // decode the token to read the username and expiration timestamp
    const token_parts = this.token.split(/\./);
    const token_decoded = JSON.parse(window.atob(token_parts[1]));
    this.token_expires = new Date(token_decoded.exp * 1000);
    this.username = token_decoded.username;
  }

}