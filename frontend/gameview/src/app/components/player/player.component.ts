import { Component, OnInit, Input } from '@angular/core';
import { PlayerService } from 'src/app/services/player.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  @Input() playerId: number;
  @Input() player: any;

  public players: any;
  constructor(private playerService: PlayerService) { }

  ngOnInit(): void {
    console.log(this.player);
    if (!this.player) {
      this.playerService.get(this.playerId)
        .subscribe(
          data => {
            this.player = data.results[0];
          },
          error => {
            console.log(error);
          });
    }
  }

}
