import { Component, OnInit } from '@angular/core';
import { PlayerStatusService } from 'src/app/services/player-status.service';

@Component({
  selector: 'app-player-board',
  templateUrl: './player-board.component.html',
  styleUrls: ['./player-board.component.css']
})
export class PlayerBoardComponent implements OnInit {
  players: any[];

  constructor(private playerStatusService: PlayerStatusService) { }

  ngOnInit(): void {
    this.retrievePlayers()

  }

  retrievePlayers(): void {
    this.playerStatusService.get()
      .subscribe(
        data => {
          this.players = data.results
        },
        error => {
          console.log(error);
        }
      );
  }

}
