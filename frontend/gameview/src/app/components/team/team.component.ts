import { Component, OnInit, Input } from '@angular/core';
import { TeamService } from 'src/app/services/team.service';
import { PlayerService } from 'src/app/services/player.service';
import { RouterOutlet, Router, ActivatedRoute, ParamMap } from '@angular/router';

const options = [
  { value: 0.95, name: '95 percentile' },
  { value: 0.75, name: '75 percentile' },
  { value: 0.5, name: 'median' },
  { value: 0, name: 'all' },
]

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})

export class TeamComponent implements OnInit {
  team: any;
  teamId: number;
  players: any[];
  options: any[];
  selectedOption: any;
  count: any;

  constructor(private playerService: PlayerService, private teamService: TeamService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.options = options;
    this.route.queryParams.subscribe(params => {
      this.teamId = params['teamId'];
    });
    this.retrieveTeam()

  }

  // getAnimationData(outlet: RouterOutlet) {
  //   return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  // }

  retrieveTeam(): void {
    this.teamService.get(this.teamId)
      .subscribe(
        data => {
          this.team = data.results && data.results[0];
          this.selectedOption = { value: 0, name: 'all' };
          this.retrievePlayer(0);
        },
        error => {
          console.log(error);
        }
      );
  }

  retrievePlayer(percetile: number): void {
    this.playerService.getByTeam(this.teamId, percetile)
      .subscribe(
        data => {
          this.players = data.results;
          console.log(this.players);
        },
        error => {
          console.log(error);
        }
      );
  }

}
