import { Component, OnInit, Input } from '@angular/core';
import { RouterOutlet, Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-score-card',
  templateUrl: './score-card.component.html',
  styleUrls: ['./score-card.component.css']
})
export class ScoreCardComponent implements OnInit {

  @Input() game: any;

  winner: any;

  constructor() { }

  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  ngOnInit() {
    const roundStyle = { 'color': 'red', 'font-size': '26px' };
    if (this.game.winner_id === this.game.team1.id) {
      this.winner = [roundStyle, {}]
    } else {
      this.winner = [{}, roundStyle]
    }
    console.log({ game: this.game, winner: this.winner })
  }

}
