import { Component, OnInit } from '@angular/core';
import { ScoreBoardService } from 'src/app/services/score-board.service';

@Component({
  selector: 'app-score-board',
  templateUrl: './score-board.component.html',
  styleUrls: ['./score-board.component.css']
})
export class ScoreBoardComponent implements OnInit {

  games: any[];
  sections: any;
  currentIndex = -1;
  teamId = null;
  playerId = null;

  constructor(private ScoreBoardService: ScoreBoardService) { }

  ngOnInit(): void {
    this.retrieveGames();
  }

  groupGamesByRound(): void {
    const sections = {}
    this.games.reduce((pre, cur) => {
      if (cur.round in pre) {
        pre[cur.round].push(cur)
      } else {
        pre[cur.round] = [cur]
      }
      return pre
    }, sections);
    this.sections = Object.entries(sections)
    console.log(this.sections);
  }

  retrieveGames(): void {
    this.ScoreBoardService.getAll()
      .subscribe(
        data => {
          this.games = data.results;
          // console.log(this.games);
          this.groupGamesByRound();
        },
        error => {
          console.log(error);
        });
  }

  searchTeam(teamId): void {
    this.ScoreBoardService.byTeam(this.teamId)
      .subscribe(
        data => {
          this.games = data.results;
          console.log(data);
          // this.groupGamesByRound()
        },
        error => {
          console.log(error);
        });
  }

  searchPlayer(teamId): void {
    this.ScoreBoardService.byPlayer(this.playerId)
      .subscribe(
        data => {
          this.games = data.results;
          console.log(data);
          // this.groupGamesByRound()
        },
        error => {
          console.log(error);
        });
  }
}
