import { Component, OnInit, Input } from '@angular/core';
import { TeamService } from 'src/app/services/team.service';
import { RouterOutlet } from '@angular/router'
import { slideInAnimation } from '../../animations';

@Component({
  selector: 'app-team-board',
  templateUrl: './team-board.component.html',
  styleUrls: ['./team-board.component.css'],
  animations: [slideInAnimation]
})
export class TeamBoardComponent implements OnInit {
  teams: any[];
  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
    this.retrieveTeam()
  }

  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  retrieveTeam(): void {
    this.teamService.getAll()
      .subscribe(
        data => {
          this.teams = data.results;
          console.log(this.teams);
        },
        error => {
          console.log(error);
        }
      );
  }

}
