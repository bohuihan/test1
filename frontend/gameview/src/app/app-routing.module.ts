import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScoreBoardComponent } from './components/score-board/score-board.component';
import { TeamBoardComponent } from './components/team-board/team-board.component';
import { TeamComponent } from './components/team/team.component';
import { PlayerBoardComponent } from './components/player-board/player-board.component';
import { PlayerComponent } from './components/player/player.component';

// import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

const routes: Routes = [
  {
    path: "score_board",
    component: ScoreBoardComponent,
  },
  {
    path: "team_board",
    component: TeamBoardComponent,
  },
  {
    path: "team",
    component: TeamComponent,
  },
  {
    path: "player_board",
    component: PlayerBoardComponent,
  },
  {
    path: "player",
    component: PlayerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
