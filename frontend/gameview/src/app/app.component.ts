import { Component } from '@angular/core';
import { UserService } from './user.service';
import { throwError } from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from './animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [slideInAnimation]
})
export class AppComponent {
  title = 'gameview';
  /**
   * An object representing the user for the login form
   */
  public user: any;
  public name: string;

  constructor(private _userService: UserService, private route: ActivatedRoute) { }

  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation;
  }

  ngOnInit() {
    this.user = {
      username: '',
      password: ''
    };
    this.route.queryParams.subscribe(params => {
      this.name = params['name'];
    });
  }

  authenticated() {
    return this._userService.token;
  }

  login() {
    this._userService.login({ 'username': this.user.username, 'password': this.user.password });
  }

  refreshToken() {
    this._userService.refreshToken();
  }

  logout() {
    this._userService.logout();
  }
}
