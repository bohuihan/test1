export const environment = {
  production: true,
  // temporarily
  baseUrl: 'http://localhost:8000',
  wsBaseUrl: 'ws://localhost:8000'
};
