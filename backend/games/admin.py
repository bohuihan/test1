from django.contrib import admin
from games.models import Game


class UserInforAdmin(admin.ModelAdmin):
    pass


class TeamAdmin(admin.ModelAdmin):
    pass


class GameAdmin(admin.ModelAdmin):
    pass


admin.site.register(Game, GameAdmin)
