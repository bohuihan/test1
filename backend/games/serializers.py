from games.models import GamePlayer, Game
from rest_framework import serializers
from users.serializers import TeamIdNameSerializer
from rest_framework.reverse import reverse


# class TeamHyperlink(serializers.HyperlinkedRelatedField):
#     # We define these as class attributes, so we don't need to pass them as arguments.
#     view_name = 'team_detail'
#     # queryset = Customer.objects.all()
#
#     def get_url(self, obj, view_name, request, format):
#         url_kwargs = {
#             'pk': obj.pk
#         }
#         return reverse(view_name, kwargs=url_kwargs, request=request, format=format)
#
#     def get_object(self, view_name, view_args, view_kwargs):
#         lookup_kwargs = {
#            'pk': view_kwargs['pk']
#         }
#         return self.get_queryset().get(**lookup_kwargs)


class GameSerializer(serializers.HyperlinkedModelSerializer):
    # serializer_related_field = TeamHyperlink
    class Meta:
        model = Game
        fields = ['id', 'team1', 'team2', 'score1', 'score2', 'round', 'winner_id']

    winner_id = serializers.IntegerField(allow_null=True)
    team1 = TeamIdNameSerializer()
    team2 = TeamIdNameSerializer()
