from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _


class Game(models.Model):
    team1 = models.ForeignKey('users.Team', related_name="team1_games", on_delete=models.CASCADE )
    team2 = models.ForeignKey('users.Team', related_name="team2_games", on_delete=models.CASCADE)
    score1 = models.IntegerField()
    score2 = models.IntegerField()
    round = models.IntegerField() # max 4 round from 16 to champion
    players = models.ManyToManyField('users.GameUser', related_name='games', through='GamePlayer')

    def __str__(self):
        return f'{self.team1}<{self.score1}> VS {self.team2}<{self.score2}>'

    def winner(self):
        if self.score1 > self.score2:
            return self.team1
        elif self.score2 > self.score1:
            return self.team2
        else:
            # draw
            raise ValueError("Cannot happen for no winner")

    def winner_name(self):
        return self.winner().name

    def winner_id(self):
        return self.winner().id

    def team1_name(self):
        return self.team1.name

    def team2_name(self):
        return self.team2.name


class GamePlayer(models.Model):
    game = models.ForeignKey(Game, related_name="game_players", on_delete=models.CASCADE)
    player = models.ForeignKey('users.GameUser', related_name="player_games", on_delete=models.CASCADE)
    score = models.IntegerField()
    # duration = models.IntegerField()
