
from rest_framework import viewsets
from rest_framework import permissions

from .models import Game
from .serializers import GameSerializer


class GameViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Game.objects.all().order_by('round', 'team1').select_related('team1', 'team2').prefetch_related('players')
    serializer_class = GameSerializer
    permission_classes = [permissions.IsAuthenticated]

