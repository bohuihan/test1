from django.test import TestCase
from .models import Game


class GameUserTest(TestCase):
    """ Test module for Puppy model """
    fixtures = ['users', 'games']

    def test_winner(self):
        gu = Game.objects.get(id=2)
        self.assertEqual(gu.team1.id, 3)
        self.assertEqual(gu.team2.id, 4)
        self.assertEqual(gu.score1, 162)
        self.assertEqual(gu.score2, 68)
        self.assertEqual(gu.winner_id(), 3)
