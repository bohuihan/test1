import math
import functools

import os
import logging

FORMAT = "%(asctime)s [%(name)s][%(levelname)s] %(message)s"


def get_logger(name, level=os.environ.get("LOGLEVEL", "INFO")):
    logging.basicConfig(format=FORMAT)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    return logger


# {{{ http://code.activestate.com/recipes/511478/ (r1)
def percentile(values, percent, key=lambda x: x):
    """
    Find the percentile of a list of values.

    @parameter values - is a list of values. Note values MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of values.

    @return - the percentile of the values
    """
    if not values:
        return None
    k = (len(values)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(values[int(k)])
    d0 = key(values[int(f)]) * (c-k)
    d1 = key(values[int(c)]) * (k-f)
    return d0+d1


# median is 50th percentile.
median = functools.partial(percentile, percent=0.5)
# end of http://code.activestate.com/recipes/511478/ }}}
