import random
import statistics
import datetime
from itertools import chain

from django.db import models
from django.db.models import Avg
from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save
from django.utils.translation import ugettext as _
from guardian.shortcuts import assign_perm
from model_utils.fields import MonitorField, StatusField
from model_utils import Choices, FieldTracker
from actstream import action

from utils import percentile


class GameUserQuerySet(models.query.QuerySet):
    def get_percentile_queryset(self, limit=0):
        queryset = self.prefetch_related('player_games')
        all_players = sorted([(p.avg_score(), p.id) for p in queryset])
        percent_value = percentile([p[0] for p in all_players], float(limit))
        valid_ids = [p[1] for p in all_players if p[0] >= percent_value]
        return queryset.filter(id__in=valid_ids)


class GameUserManager(UserManager):
    def get_queryset(self):
        return GameUserQuerySet(model=self.model, using=self._db, hints=self._hints)


class GameUser(AbstractUser):
    ONLINE_CHOICES = Choices('online', 'offline')
    # only for players
    team = models.ForeignKey("Team", related_name="players", on_delete=models.CASCADE, blank=True, null=True)
    height = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, default=None)

    status = StatusField(choices_name='ONLINE_CHOICES', default=ONLINE_CHOICES.offline)
    status_changed_at = MonitorField(monitor='status')
    total_logged_seconds = models.FloatField(default=0.0)
    total_logged_count = models.IntegerField(default=0)

    tracker = FieldTracker()

    objects = GameUserManager()

    class Meta:
        permissions = (
            ('manage_player', 'Manage player'),
        )

    def __str__(self):
        return self.username

    def avg_score(self):
        return self.player_games.aggregate(avg_score=Avg('score'))['avg_score'] or 0.00

    def game_count(self):
        return self.player_games.count()


@receiver(pre_save, sender=GameUser)
def create_activity(sender, **kwargs):
    """
    log user status
    """
    user = kwargs["instance"]
    if user.tracker.has_changed('status') and user.tracker.previous('status_changed_at') and \
            user.status == GameUser.ONLINE_CHOICES.offline:
        delta = datetime.datetime.now(datetime.timezone.utc) - user.tracker.previous('status_changed_at')
        activity_duration = delta.total_seconds()
        action.send(user, verb='active_for', duration=activity_duration)
        user.total_logged_seconds += activity_duration
        user.total_logged_count += 1


@receiver(post_save, sender=GameUser)
def create_permission(sender, **kwargs):
    """
    Create a permission instance for all newly created User instances.
    """
    user, created = kwargs["instance"], kwargs["created"]
    if created and user.username != getattr(settings, 'ANONYMOUS_USER_NAME', None):
        assign_perm("manage_player", user, user)


class Team(models.Model):
    # some assumption:
    # one coach can only coach one team
    name = models.CharField(_('name'), max_length=150, unique=True)
    coach = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="own_team", on_delete=models.CASCADE, null=False)

    class Meta:
        permissions = (
            ('manage_team_players', 'Manage team players'),
        )

    def __str__(self):
        return self.name

    def pick_player(self, count):
        total_count = self.players.count()
        if count > total_count:
            raise Exception(f'Cannot pick {count} users from ${total_count} users')

        return random.sample([p for p in self.players.all()], count)

    def avg_score(self):
        return statistics.mean(chain(
            self.team1_games.values_list('score1', flat=True),
            self.team2_games.values_list('score2', flat=True)
        ))

    def players_by_percentile(self, percent=0.90):
        return self.players.all().get_percentile_queryset(percent)


@receiver(post_save, sender=Team)
def team_post_save(sender, **kwargs):
    """
    Create a permission instance for all newly created User instances.
    """
    team, created = kwargs["instance"], kwargs["created"]
    if created:
        assign_perm("manage_team_players", team.coach, team)
