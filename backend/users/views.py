
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import generics
from rest_framework import mixins
from django.views.generic.base import TemplateView
from django.views.generic import ListView

from .models import GameUser, Team
from .serializers import UserSerializer, TeamSerializer, TeamIdNameSerializer, UserStatusSerializer

from .permissions import IsTeamManger, IsSuper


class PlayerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = GameUser.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsTeamManger]

    def get_queryset(self):
        """
        Optionally restricts the returned players to a given user,
        by filtering against team id in the URL.
        """
        queryset = self.queryset
        team_id = self.request.query_params.get('team_id')
        if team_id:
            queryset = queryset.filter(team_id=team_id)
        limit = self.request.query_params.get('percentile')
        if limit:
            queryset = queryset.get_percentile_queryset(limit)
        else:
            queryset = self.queryset

        return queryset


class PlayerStatusViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = GameUser.objects.all()
    serializer_class = UserStatusSerializer
    permission_classes = [IsSuper]


class TeamViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows teams to be viewed or edited.
    """
    queryset = Team.objects.all()
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        """
        Optionally restricts the returned players to a given user,
        by filtering against team id in the URL.
        """
        queryset = self.queryset
        if self.request.query_params.get('team_id'):
            queryset = queryset.filter(id=self.request.query_params['team_id'])
            self.serializer_class = TeamSerializer
            self.permission_classes = [IsTeamManger]
        else:
            self.serializer_class = TeamIdNameSerializer

        return queryset



# class UserListView(ListView):
#     model = GameUser
#     paginate_by = 100
#     permission_classes = [IsTeamManger]
#     template_name = "team_list.html"
#
#     def head(self, *args, **kwargs):
#         last_book = self.get_queryset().latest('publication_date')
#         response = HttpResponse()
#         # RFC 1123 date format
#         response['Last-Modified'] = last_book.publication_date.strftime('%a, %d %b %Y %H:%M:%S GMT')
#         return response

# class TeamDetailView(TemplateView):
#     # queryset = Team.objects.all()
#     # serializer_class = TeamSerializer
#     permission_classes = [IsTeamManger]
#     template_name="team.html"
#
#     def get_context_data(self, **kwargs):
#         import pdb;pdb.set_trace()
#         context = super().get_context_data(**kwargs)
#         context['team'] = Team.objects.get(self.kwargs['pk'])
#         return context

    # def get(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     return
