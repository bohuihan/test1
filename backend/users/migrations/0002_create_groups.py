from django.db import migrations


def apply_migration(apps, schema_editor):
    db_alias = schema_editor.connection.alias

    Group = apps.get_model("auth", "Group")
    Group.objects.using(db_alias).bulk_create(
        [Group(name="admin"), Group(name="player"), Group(name="coach")]
    )

    # use new permission to control
    # can_fm_list = Permission.objects.get(name='can_fm_list')
    # newgroup.permissions.add(can_fm_list)


def revert_migration(apps, schema_editor):
    Group = apps.get_model("auth", "Group")
    Group.objects.filter(name__in=["admin", "player", "couch"]).delete()


class Migration(migrations.Migration):
    dependencies = [("users", "0001_initial")]
    operations = [migrations.RunPython(apply_migration, revert_migration)]
