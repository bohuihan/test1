import random
import math
import logging

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User, Group
from django.contrib.auth.hashers import make_password
from django.db import transaction

from users.models import GameUser, Team
from games.models import Game, GamePlayer

from utils import get_logger


TEAM_COUNT = 16
TEAM_SIZE = 10

MIN_PLAYER_COUNT = 5
MAX_PLAYER_COUNT = TEAM_SIZE
MAX_PLAYER_SCORE = 30

logger = get_logger('create_user', level='DEBUG')


class Command(BaseCommand):
    help = 'Displays current time'

    @staticmethod
    @transaction.atomic
    def create_user_teams():
        coaches = []
        playes = []
        teams = []
        logger.info('start create users and teams')

        for team_index in range(TEAM_COUNT):
            team_name = f'team{team_index}'
            teams.append({
                "name": team_name
            })
            coaches.append({
                'username': f'{team_name}_coach',
                'email': 'user@email.com',
                'password': make_password('Password!'),
                'is_active': True,
                'is_staff': True,
                'total_logged_seconds': random.randint(170, 250),
                'total_logged_count': random.randint(0, 20),
                'status': random.choices(['online', 'offline'])[0]
            })
            for player_index in range(TEAM_SIZE):
                playes.append({
                    'username': f'{team_name}_player{player_index}',
                    'email': 'user@email.com',
                    'password': make_password('Password!'),
                    'is_active': True,
                    'height': random.randint(170, 250),
                    'total_logged_seconds': random.randint(170, 250),
                    'total_logged_count': random.randint(0, 20),
                    'status': random.choices(['online', 'offline'])[0]
                })

        logger.info('prepare done')
        logger.debug(coaches, playes, teams)

        logger.info('create coaches')
        GameUser.objects.bulk_create(
            [GameUser(**u) for u in coaches]
        )
        coach_group = Group.objects.get(name='coach')

        all_coaches = [c for c in GameUser.objects.all().order_by('id')]
        coach_group.user_set.add(*all_coaches)

        logger.info('create admin')
        GameUser.objects.create_superuser('admin', 'admin@email.com', 'admin')

        logger.info('create teams')
        all_teams = [Team.objects.create(**t, coach=all_coaches[i]) for i, t in enumerate(teams)]

        logger.info('create players')
        player_group = Group.objects.get(name='player')
        for i, u in enumerate(playes):
            player = GameUser(**u, team=all_teams[i // TEAM_SIZE])
            player.save()
            player.groups.add(player_group)

        logger.info('finished creating users and teams')

    @staticmethod
    @transaction.atomic
    def create_games():
        logger.info('start create games')
        round_count = math.ceil(math.log(TEAM_SIZE, 2))

        last_winner = [t for t in Team.objects.all()]

        for i in range(round_count):
            logger.debug(f'round: {i}: teams: {last_winner}')
            team_size = len(last_winner)
            if team_size % 2:
                next_winner = [last_winner[-1]]
            else:
                next_winner = []

            for j in range(team_size // 2):
                team1 = last_winner[j*2]
                team2 = last_winner[j*2+1]

                players1 = team1.pick_player(random.randint(MIN_PLAYER_COUNT, MAX_PLAYER_COUNT))
                player1_score = [random.randint(0, MAX_PLAYER_SCORE) for p in players1]

                players2 = team2.pick_player(random.randint(MIN_PLAYER_COUNT, MAX_PLAYER_COUNT))
                player2_score = [random.randint(0, MAX_PLAYER_SCORE) for p in players2]

                team1_score = sum(player1_score)
                team2_score = sum(player2_score)
                if team1_score == team2_score:
                    player1_score[0] += random.randint(10) # let team1 won always

                game = Game.objects.create(round=i+1, team1=team1, team2=team2, score1=team1_score, score2=team2_score)
                players = players1 + players2
                scores = player1_score + player2_score

                GamePlayer.objects.bulk_create([
                    GamePlayer(game=game, player=player, score=scores[i]) for i, player in enumerate(players)
                ])

                winner = game.winner()
                next_winner.append(winner)
            last_winner = next_winner

        logger.info('finished creating games')

    #
    def handle(self, *args, **kwargs):
        self.create_user_teams()
        self.create_games()



