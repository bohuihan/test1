from users.models import Team, GameUser
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GameUser
        fields = ['id', 'username', 'team', 'height', 'avg_score', 'game_count']


class UserStatusSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = GameUser
        fields = ['id', 'username', 'status', 'total_logged_seconds', 'total_logged_count', 'status_changed_at']


class UserNameIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameUser
        fields = ['id', 'username']


class TeamSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Team
        fields = ['id', 'name', 'coach', 'avg_score']
    #
    coach = UserNameIdSerializer()
    # players = UserNameIdSerializer(many=True, read_only=True)


class TeamIdNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ['id', 'name']
