import json
import uuid
import time
from channels.generic.websocket import AsyncConsumer
from channels.db import database_sync_to_async
from django.contrib.auth import get_user_model

from enum import Enum

class MesssageType(Enum):
    MESSAGE = "MESSAGE"
    ACTION = "ACTION"


class ConnectConsumer(AsyncConsumer):

    async def websocket_connect(self, event):
        # Called when a new websocket connection is established
        print("connected", event)
        user = self.scope['user']
        self.update_user_status(user, 'online')

    async def websocket_receive(self, event):
        # Called when a message is received from the websocket
        # Method NOT used
        print("received", event)

    async def websocket_disconnect(self, event):
        # Called when a websocket is disconnected
        print("disconnected", event)
        user = self.scope['user']
        self.update_user_status(user, 'offline')

    @database_sync_to_async
    def update_user_status(self, user, status):
        """
        Updates the user `status.
        `status` can be one of the following status: 'online', 'offline'
        """
        user.status = status
        # make sure post save signal is triggered
        user.save()
