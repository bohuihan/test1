from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig


class UsersConfig(AppConfig):
    name = 'users'

    def ready(self):
        from actstream import registry
        registry.register(self.get_model('GameUser'))


# class GameUserAdminConfig(AdminConfig):
#     default_site = 'users.admin.GameUserAdminSite'
