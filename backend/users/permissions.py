from rest_framework.permissions import BasePermission
from users.models import Team


class IsTeamManger(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        if bool(request.user and request.user.is_superuser):
            return True

        if request.query_params.get('team_id'):
            team = Team.objects.get(id=request.query_params['team_id'])
            return bool(request.user and request.user.has_perm('manage_team_players', team))

        return False


class IsSuper(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        if bool(request.user and request.user.is_superuser):
            return True
        return False
