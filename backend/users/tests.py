from django.test import TestCase
from .models import GameUser, Team


class GameUserTest(TestCase):
    """ Test module for GameUser model """
    fixtures = ['users', 'games']

    def test_avg_score(self):
        gu = GameUser.objects.get(id=121)
        self.assertEqual([i for i in gu.player_games.values_list('score', flat=True)], [27, 23, 3])
        self.assertEqual(gu.avg_score(), 17.666666666666668)

    def test_game_count(self):
        gu = GameUser.objects.get(id=121)
        self.assertEqual(gu.game_count(), 3)


class GameUserTest(TestCase):
    """ Test module for Team model """
    fixtures = ['users', 'games']

    def test_avg_score(self):
        team = Team.objects.get(id=11)
        self.assertEqual([i for i in team.team1_games.values_list('score1', flat=True)], [114, 159])
        self.assertEqual([i for i in team.team2_games.values_list('score2', flat=True)], [156, 168])
        self.assertEqual(team.avg_score(), 149.25)

    def test_game_count(self):
        team = Team.objects.get(id=2)
        self.assertEqual([(p.id, p.avg_score()) for p in team.players.all()],
                         [(28, 19.5),
                          (29, 6.0),
                          (30, 12.5),
                          (31, 5.0),
                          (32, 17.0),
                          (33, 6.0),
                          (34, 9.0),
                          (35, 13.0),
                          (36, 23.0),
                          (37, 11.5)
                          ])
        self.assertEqual([i for i in team.players_by_percentile().values_list('id', flat=True)], [36])

