from django.contrib import admin
from django.contrib.admin import SimpleListFilter

from users.models import GameUser
from users.models import Team


admin.AdminSite.enable_nav_sidebar = False


# TODO: saved into new file
class PercentileFilter(SimpleListFilter):
    title = 'Percentile'
    parameter_name = 'percentile'

    def lookups(self, request, model_admin):
        return [
            (0.95, '95 percentile'),
            (0.75, '75 percentile'),
            (0.5, 'median'),
        ]

    def queryset(self, request, queryset):
        if not self.value():
            return queryset

        return queryset.get_percentile_queryset(self.value())


# class ReadAdminMixin():
#     def has_add_permission(self, request):
#         return False
#
#     def has_change_permission(self, request, obj=None):
#         return False
#
#     def has_delete_permission(self, request, obj=None):
#         return False
#
#     def has_view_permission(self, request, obj=None):
#         if bool(request.user and request.user.is_staff):
#             return True
#
#         if request.query_params.get('team_id'):
#             team = Team.objects.get(id=request.query_params['team_id'])
#             return bool(request.user and request.user.has_perm('manage_team_players', team))
#
#         return False


class GameUserAdmin(admin.ModelAdmin):
    # fields = ['username', 'height', 'avg_score', 'game_count']
    list_filter = ('team', PercentileFilter)
    readonly_fields = ['avg_score', 'game_count', 'status']
    extra = 0

    def has_delete_permission(self, request, obj=None):
        return False


class GameUserInline(admin.TabularInline):
    model = GameUser
    fields = ['username', 'height', 'avg_score', 'game_count']
    readonly_fields = ['username', 'height', 'avg_score', 'game_count']
    extra = 0

    def has_delete_permission(self, request, obj=None):
        return False


class TeamAdmin(admin.ModelAdmin):
    inlines = [
        GameUserInline
    ]


admin.site.register(GameUser, GameUserAdmin)
admin.site.register(Team, TeamAdmin)
